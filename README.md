# MicroPython-based power strip control system #

I had design a system for control of cheap power strips/sockets for a laboratory.
I decided to use wirelessly controlled power strips/sockets for the following reasons:
  - the wirelessly controlled power strips/sockets are much cheaper than those equipped with Ethernet,
  - there is no need to route the control cable to each power strip/socket,
  - there are no problems with isolation between the control cable and the mains wires.

The right solution maybe the WiFi-controlled power strips/sockets. Their significant disadvantage
is a necessity to use a remote server. Such a solution was not acceptable for the lab.
The sockets must be controlled via systems remaining fully under control of the user.
Fortunately, most WiFi-controled sockets are based on ESP8266 or ESP8285 SoC and there very nice alternative 
firmwares (e.g., [Tasmota](https://github.com/arendst/Tasmota) or [Espurna](https://github.com/xoseperez/espurna)) that allow the user to avoid necessity to connect the socket 
to an alien server. When using them, the controlled socket must connect to the local WiFi access point,
so it must store the WiFi password.

There were however other limitations resulting from the local policy and security reasons:
  - The WiFi used to control sockets should not be visible when not in use
  - The users are not allowed to create WiFi networks connected to the institutional network
  - Compromising of a controlled socket (e.g., due to theft and reverse engineering) should not result in loss of security of the whole network.

Basing on the above requirements, I have decided to build the system with the following architecture:
  - Each controlled power strip/socket:
     - is reflashed with [MicroPython](https://docs.micropython.org/en/latest/esp8266/tutorial/intro.html).
     - works as a separate WiFi AP with individual ESSID, which is not broadcast and is WPA2-PSK protected (so compromising any socket does not affect others or the whole network).
     - runs a TCP/IP server that accepts JSON or Msgpack encapsulated messages with commands to read or set the state of the controlled socket (or sockets in case of power strips)
     - it also provides the MicroPython webrepl, protected with yet another individual password. It allows to stop the operation of TCP/IP server and modify from the remote the configuration of the socket/power strip

Accessing the individual sockets/power strips via their individual WiFi networks from a computer or WiFi-enabled router may be difficult and may create disallowed connections between the WiFi and institutional network. Therefore the controller is implemented on a separate ESP8266- or ESP32-based device, which also runs MicroPython. Connection to a WiFi network from MicroPython is trivial (I ommit the necessary error checking, and assume that the server uses a standard MicroPython's 192.168.4.1 IP address):

```
w=network.WLAN(network.STA_IF)
w.connect(‘essid’,’password’)
s=usocket.socket(usocket.AF_INET,usocket.SOCK_STREAM) 
s.connect(("192.168.4.1","80"))
s.sendall(control_message)
res=s.recv(MAX_RESP_LEN)
s.close()
w.disconnect()
```

The controller device is connected to the computer's or router's USB port as USB-UART adapter, and accepts JSON or Msgpack encapsulated control messages.

More information is available in Wiki
- [List](../../wikis/Tested-power-strips-or-sockets) of tested power strips and sockets
